import pandas as pd
import glob, os, sys
import math, numbers
import pickle
import argparse
from bs4 import BeautifulSoup
import re
import pycorenlp
import nltk
import spacy
from nltk.tag import StanfordNERTagger
from nltk.tokenize import word_tokenize

from bs4 import BeautifulSoup

from itertools import groupby
import pathlib

import warnings

warnings.filterwarnings("ignore")


# initialize necessary tools for executing stanford model
# nltk.download('punkt')


dp_names = ["MEETING_ADD_STREET", "MEETING_ADD_CITY",
            "MEETING_ADD_STATE", "MEETING_ADD_ZIP", "MEETING_ADD_COUNTRY"]

model_groups = {
  "MEETING_ADD_STREET": "g2",
  "MEETING_ADD_CITY": "g2",
  "MEETING_ADD_STATE": "g2",
  "MEETING_ADD_ZIP": "g2",
  "MEETING_ADD_COUNTRY": "g2",
}

model_gp_to_name_dict = {
#  "g1": "time_timezone_type.ser.gz",
  "g2": "8_class.ser.gz"
#  "g3": "attempt2.ser.gz"
}


accuracy_dict = {
       "MEETING_ADD_STREET" : 0,
        "MEETING_ADD_CITY": 0,
       "MEETING_ADD_STATE": 0,
        "MEETING_ADD_ZIP": 0,
        "MEETING_ADD_COUNTRY": 0,
        "COUNT": 0,
        "LOCALIZED_COUNT":0,
        "EXCEPTION_COUNT":0,
        "VALUE_OFFSET_MISMATCH":0,
        "MISSING_GOLD": 0,
        "BOTH_ABSENT": 0,
        "VIRTUAL": 0
    }
logs_data = {'DataPoint': [], 'Expected': [], 'Actual': [], 'Localization': [], 'ExceptionCategory': [],
             'Exception': [], 'File': []}
# Convert the dictionary into DataFrame
df = pd.DataFrame(logs_data)


def split_address(file_address):
  path = pathlib.Path(file_address)
  directory_address = path.parent


# initialize the gold standard offsets in a dataframe
def initialize_dataframe(gold_standard_file):
  file_location = gold_standard_file
  reference_sheet = "Document"
  target_sheet = "T_PXYP_MEETING"

  sheet_to_df_map = pd.read_excel(file_location, sheet_name=None)
  sheet_to_df_map[target_sheet].set_index('RFID_SEQ_NUM', inplace=True)

  filename_to_ref_no = {}
  for index, row in sheet_to_df_map[reference_sheet].iterrows():
    filename_to_ref_no[row["SEC_ACCESS_NUM"] + ".txt"] = row["RFID_SEQ_NUM"]

  sheet_to_df_map[reference_sheet].set_index('RFID_SEQ_NUM', inplace=True)
  return filename_to_ref_no, sheet_to_df_map


# check if result is dataframe or series and return accordingly
def get_target_row(rfid, sheet_to_df_map, target_sheet):
  target_variable = sheet_to_df_map[target_sheet].loc[rfid]
  if isinstance(target_variable, pd.DataFrame):
    return target_variable.iloc[0]
  return target_variable


def construct_regex_for_seq_match(word_list):
  final_string = ""
  for i in range(len(word_list) - 1):
    temp_string = "(" + word_list[i] + ")" + "(?:(?!(" + '|'.join(
      word for word in word_list[:i + 1] + word_list[i + 2:]) + ")).)*?"
    final_string += temp_string
  return final_string + "(" + word_list[-1] + ")"


def get_offsets(parent_string, dp_str, reference_offset):
  c_reg = re.compile(construct_regex_for_seq_match(dp_str.split(" ")))
  m = c_reg.search(parent_string)
  return m.span()[0] + reference_offset, m.span()[1] + reference_offset


def get_stanford_model(model_name):
  path = r'%s' % os.getcwd().replace('\\', '/')
  parent_directory_location = path + "/models/stanford-ner-tagger/"
  model_sub_directory_name = "classifiers/"
  stanford_jar_file_name = "stanford-ner-3.9.2.jar"

  stanford_model = StanfordNERTagger(parent_directory_location + model_sub_directory_name + model_name,
                                     parent_directory_location + stanford_jar_file_name,
                                      encoding='utf-8')


  return stanford_model

def convert_htmlstring_to_tokens(html_string):
  html_string = html_string.replace('\\n', '\n')
  soup = BeautifulSoup(html_string, "lxml")
  text = soup.get_text(separator=u' ')
  tokenized_string = word_tokenize(text)
  return tokenized_string, text


def filter_classified_tokens(classified_text, result_dict, dp_names):
  for tag, chunk in groupby(classified_text, lambda x: x[1]):
    if tag != "O":
      if tag not in result_dict and tag in dp_names:
        result_dict[tag] = " ".join(w for w, t in chunk)


def find_ref(localized_string):
  p1 = re.compile('held')
  p2 = re.compile(' ?([AaPp][.][Mm])')
  p3 = re.compile('([0-1]?[0-9]|2[0-3]):[0-5][0-9]')
  p4 = re.compile('20\d\d')
  start_offset = None
  if p1.search(localized_string):
    start_offset = p1.search(localized_string).start()
  elif p2.search(localized_string):
    start_offset = p2.search(localized_string).start()
  elif p3.search(localized_string):
    start_offset = p3.search(localized_string).start()
  elif p4.search(localized_string):
    start_offset = p4.search(localized_string).start()
  return start_offset


def find_closest_pair(state_list, city_list, localized_string):
  if len(state_list) == 0 and len(city_list) == 0:
    return None, None, None, None
  min_distance = math.inf
  state_offset = None
  city_offset = None
  state = None
  city = None
  ref_offset = find_ref(localized_string)

  for state_item in state_list:
    for m in re.finditer(state_item, localized_string):
      if min_distance > abs(m.start() - ref_offset):
        state = state_item
        state_offset = m.start()
  min_distance = math.inf
  if not state_offset:
    cit_ref = ref_offset
  else:
    cit_ref = state_offset

  for city_item in city_list:
    for m in re.finditer(city_item, localized_string):
      if min_distance > abs(m.start() - cit_ref):
        city = city_item
        city_offset = m.start()


  if not city or (state_offset and abs(state_offset - city_offset) > 20):
    result = re.search(".*,\s*(.*)\s*,.*", localized_string[state_offset - 15: state_offset + 3])
    if result:
      city = result.group(1).strip()
      city_offset = state_offset - 15 + localized_string[state_offset-15:state_offset+3].find(city)

  if not state and city:
      city = city.strip()
      lis = localized_string[city_offset: city_offset + 30].split(',')
      if lis and len(lis) > 1:
        state = lis[1].strip()
      if state:
        state_offset = city_offset + localized_string[city_offset:city_offset + 30].find(state)


  return state, city, state_offset, city_offset


def find_missing_entity(state_list, city_list, localized_string):
  state = None
  city = None
  if len(state_list) > 0:
    for state in state_list:
      state = state.strip()
      if state == 'NY' or state == 'New York':
        state = 'NY'
        city = 'New York'
        return state, city
      offset = localized_string.find(state)
      result = re.search(".*,\s*(.*)\s*,.*", localized_string[offset - 15 : offset + 3])
      if result:
        city = result.group(1).strip()
        return state, city
  elif len(city_list) > 0:
    for city in city_list:
      city = city.strip()
      offset = localized_string.find(city)
      lis = localized_string[offset : offset + 20].split(',')
      state = lis[1].strip()
      if len(state) < 10:
        return state, city
  return state, city

def classify_using_pretrained_NER(localized_string, results, df, filename):

  # convert localized_string to tokens
  tokenized_string, normal_text = convert_htmlstring_to_tokens(localized_string)
  model = pycorenlp.StanfordCoreNLP('http://localhost:9000')
  properties = {'annotators': 'tokenize, ssplit, pos, lemma,ner',
                'outputFormat':'json'}

  tex = normal_text.replace('\n', ' ').replace('\xa0', '').replace('\\n', ' ').strip()
  tex = re.sub('\s+', ' ', tex)
  tex = re.sub('\Â', '', tex)
  tex = re.sub('[^A-Za-z0-9\s,]+', '', tex)


  result_json = model.annotate(tex, properties)
  objects = result_json['sentences'][0]['entitymentions']
  state_count = 0
  city_count = 0
  state_list = []
  city_list = []
  state_present = True if 'MEETING_ADD_STATE' in results.keys() else False
  city_present = True if 'MEETING_ADD_CITY' in results.keys() else False


  for object in objects:
    if not state_present and object['ner'] == 'STATE_OR_PROVINCE':
      state_list.append((object['text']))
      #results['MEETING_ADD_STATE'] = object['text']
      state_count += 1
    elif not city_present and object['ner'] == 'CITY':
      city_list.append((object['text']))
      city_count += 1
  if not state_present and state_count == 0:
    df.loc[get_index()] = ['MEETING_ADD_STATE', None, None, normal_text, 'State Absent', None, filename]
  if not city_present and city_count == 0:
    df.loc[get_index()] = ['MEETING_ADD_CITY', None, None, normal_text, 'City Absent', None, filename]
  if state_present:
    state_list = [results['MEETING_ADD_STATE']]
  if city_present:
    city_list = [results['MEETING_ADD_CITY']]
  if not city_present or not state_present:
    state_result, city_result, state_offset, city_offset = find_closest_pair(list(set(state_list)), list(set(city_list)), localized_string)
    if state_result:
      results['MEETING_ADD_STATE'] = state_result
    if city_result:
      results['MEETING_ADD_CITY'] = city_result
#  elif state_count + city_count > 0:
#    results['MEETING_ADD_STATE'], results['MEETING_ADD_CITY'] = find_missing_entity(state_list, city_list, localized_string)
#  print('state_count {} city count {}'.format(state_count, city_count))



# input: localized html_string
# returns the tagged words for each datapoints in dictionary
# logs are stored in this function
def classify_text_for_pxy_info_dp(localized_string):

  model_to_object_dict = {}
  result_string_dict = {}

  # initialize by loading the required models
  for dp_name in dp_names:
    model_name = model_gp_to_name_dict[model_groups[dp_name]]
    if model_name not in model_to_object_dict:
      model_to_object_dict[model_name] = get_stanford_model(model_name)

  # convert localized_string to tokens
  tokenized_string, text = convert_htmlstring_to_tokens(localized_string)

  # classify the string for all the dp's using the loaded models
  for dp_group in model_gp_to_name_dict:
    model_name = model_gp_to_name_dict[dp_group]
    st_model = model_to_object_dict[model_name]
    classified_tokens = st_model.tag(tokenized_string)
    filter_classified_tokens(classified_tokens, result_string_dict, model_groups)

  return result_string_dict


# input- results dictionary
def save_logs(results, filename, localize_string, gold_standard_file, filename_to_ref_no, sheet_to_df_map):
  basefilename = os.path.basename(filename)

  dp_names = ["MEETING_TIME", "MEETING_TIME_ZONE", "MEETING_LOCATION", "MEETING_ADD_STREET", "MEETING_ADD_CITY",
              "MEETING_ADD_STATE", "MEETING_ADD_ZIP", "MEETING_ADD_COUNTRY", "MEETING_TYPE", "VIRTUAL_PHYSICAL"]


  reference_sheet = "Document"
  target_sheet = "T_PXYP_MEETING"
  reference_column = "START_OFFSET"
  #print(filename_to_ref_no)

  # get the essential offsets
  rfid = filename_to_ref_no[os.path.basename(filename)]
  reference_offset = sheet_to_df_map[reference_sheet].loc[rfid][reference_column]
  file = open(filename, 'r')
  file_string = file.read().replace("\n", "\\n")

  # offest of the start of localization
  calc_start_offset = file_string.find(localize_string)

  target_row = get_target_row(rfid, sheet_to_df_map, target_sheet)
  hs = open("output/output_" + basefilename.replace("txt", "html"), "a", encoding="utf-8")
  fs = open("output/failure_" + basefilename.replace("txt", "html"), "a", encoding="utf-8")

  fs.write("<h2>General Proxy Info Meeting datapoint category failures</h2><br>\n")

  header = "<h2>General Proxy Info Meeting datapoint category extraction</h2>\n<br>\n"

  hs.write(header)

  localized_section = "<hr>" + str(BeautifulSoup(localize_string.replace("\\n", "\n"), "lxml")) + "<ht><br>\n"

  hs.write(localized_section)

  hs.write("<h2>Extraction Results</h2><br>\n")

  table_start = '<table style="border-collapse: collapse;border: 1px solid black;"><tbody>'

  table_header = '<tr>\
        <th style="border: 1px solid black;">Datapoint Name</th>\
            <th style="border: 1px solid black;">Expected Value</th\>\
                <th style="border: 1px solid black;">Extracted Value</th>\
                    <th style="border: 1px solid black;">Match/Mismatch</th>\
                        <th style="border: 1px solid black;">Start Offset</th>\
                            <th style="border: 1px solid black;">End Offset</th>\
                                </tr>'

  buffer = ""
  for dp in dp_names:
    calc_s_offset = None
    calc_e_offset = None
    buffer += '<tr><td style="border: 1px solid black;">' + dp + '</td>'
    buffer += '<td style="border: 1px solid black;">' + file_string[reference_offset + int(
      target_row[dp + "_S"]):reference_offset + int(target_row[dp + "_E"])] if target_row[dp + "_S"] else "[]" + '</td>'
    if dp in results:
      buffer += '<td style="border: 1px solid black;">' + results[dp] + '</td>'

      c_reg = re.compile(construct_regex_for_seq_match(results[dp].split(" ")))
      # get the first match in the localized string
      for m in c_reg.finditer(localize_string):
        # print(file_string[calc_start_offset+m.start():calc_start_offset+m.start()+len(m.group())])
        calc_s_offset = calc_start_offset + m.start()
        calc_e_offset = calc_start_offset + m.start() + len(m.group())
        break

      if c_reg.search(
              file_string[reference_offset + int(target_row[dp + "_S"]):reference_offset + int(target_row[dp + "_E"])]):
        buffer += '<td style="border: 1px solid black;">' + "Match" + '</td>'
      else:
        buffer += '<td style="border: 1px solid black;">' + "Mismatch" + '</td>'

    elif dp == "VIRTUAL_PHYSICAL" and "MEETING_LOCATION" in results:

      c_reg = re.compile(construct_regex_for_seq_match(results['MEETING_LOCATION'].split(" ")))
      # get the first match in the localized string
      for m in c_reg.finditer(localize_string):
        # print(file_string[calc_start_offset+m.start():calc_start_offset+m.start()+len(m.group())])
        calc_s_offset = calc_start_offset + m.start()
        calc_e_offset = calc_start_offset + m.start() + len(m.group())
        break

      buffer += '<td style="border: 1px solid black;">' + results['MEETING_LOCATION'] + '</td>'
      if file_string[reference_offset + int(target_row['VIRTUAL_PHYSICAL' + "_S"]):reference_offset + int(
              target_row['VIRTUAL_PHYSICAL' + "_E"])] == results['MEETING_LOCATION']:
        buffer += '<td style="border: 1px solid black;">' + "Match" + '</td>'
      else:
        buffer += '<td style="border: 1px solid black;">' + "Mismatch" + '</td>'


    elif dp == "MEETING_ADD_COUNTRY" and "MEETING_ADD_STATE" in results:

      c_reg = re.compile(construct_regex_for_seq_match(results['MEETING_ADD_STATE'].split(" ")))
      # get the first match in the localized string
      for m in c_reg.finditer(localize_string):
        # print(file_string[calc_start_offset+m.start():calc_start_offset+m.start()+len(m.group())])
        calc_s_offset = calc_start_offset + m.start()
        calc_e_offset = calc_start_offset + m.start() + len(m.group())
        break

      buffer += '<td style="border: 1px solid black;">' + results['MEETING_ADD_STATE'] + '</td>'
      if file_string[reference_offset + int(target_row['MEETING_ADD_COUNTRY' + "_S"]):reference_offset + int(
              target_row['MEETING_ADD_COUNTRY' + "_E"])] == results['MEETING_ADD_STATE']:
        buffer += '<td style="border: 1px solid black;">' + "Match" + '</td>'
      else:
        buffer += '<td style="border: 1px solid black;">' + "Mismatch" + '</td>'
    else:
      buffer += '<td style="border: 1px solid black;">[]</td>'
      fs.write(dp + "<br>\n")
      buffer += '<td style="border: 1px solid black;">Mismatch</td>'

    buffer += '<td style="border: 1px solid black;">' + str(
      calc_s_offset) + '</td><td style="border: 1px solid black;">' + str(calc_e_offset) + '</td>'

    buffer += '</tr>'

  table_end = '</tbody></table><br>\n'

  result_html = table_start + table_header + buffer + table_end

  hs.write(result_html)
  fs.write("<hr>")

  hs.close()
  fs.close()




def compute_accuracy(results, filename, localize_string):
  target_sheet = "T_PXYP_MEETING"
  rfid = filename_to_ref_no[os.path.basename(filename)]
  target_row = get_target_row(rfid, sheet_to_df_map, target_sheet)

  for dp in dp_names:
    if not target_row[dp] or dp in results and results[dp] == target_row[dp]:
      accuracy_dict[dp] = accuracy_dict[dp] + 1
    else:
      expected = target_row[dp]
      actual = results[dp] if dp in results else ""
      index = 0
      if df.empty == False:
        index = max(df.index) + 1
      df.loc[index] = [dp, expected, actual, localize_string, None, None, filename]


def check_value(expected, actual, dp, reference_offset, file_string, localize_string, filename):
  #print('Expected String using Offsets'+ file_string[reference_offset + int(
  #    expected[dp + "_S"]):reference_offset + int(expected[dp + "_E"])])

  # if c_reg.search(file_string[reference_offset + int(expected[dp + "_S"]):reference_offset + int(expected[dp + "_E"])]):
  #   print('Matching done using -- actual offset (create regex), use that to match with expected string using offset')

  if not expected[dp] and not expected[dp+'_S']:
    accuracy_dict["MISSING_GOLD"] = accuracy_dict["MISSING_GOLD"] + 1
    return True
  if not expected[dp] or (isinstance(expected[dp], numbers.Number)  and  math.isnan(expected[dp])):
    accuracy_dict["MISSING_GOLD"] = accuracy_dict["MISSING_GOLD"] + 1
    return True
  if expected['VIRTUAL_PHYSICAL'] == 2:
    accuracy_dict["VIRTUAL"] += 1
    return True
  if dp in actual and actual[dp] == expected[dp]:
    c_reg = re.compile(construct_regex_for_seq_match(actual[dp].split(" ")))
    calc_start_offset = file_string.find(localize_string)
    start = localize_string.find(actual[dp]) + calc_start_offset
    end = start + len(actual[dp])

    # for m in c_reg.finditer(localize_string):
    #   start = calc_start_offset + m.start()
    #   end = calc_start_offset + m.start() + len(m.group())
    #   break
    if start == (reference_offset + int(expected[dp + "_S"])) and end == (
            reference_offset + int(expected[dp + "_E"])):
      return True
    else:
      accuracy_dict["VALUE_OFFSET_MISMATCH"] = accuracy_dict["VALUE_OFFSET_MISMATCH"] + 1
      tokenized_string, text = convert_htmlstring_to_tokens(localize_string)
      #df.loc[get_index()] = [dp, file_string[reference_offset + int(expected[dp + "_S"]) - 20: reference_offset + int(expected[dp + "_E"])+ 20] , file_string[start -20 : end+20], text, 'MISMATCH', None, filename]
      # if dp == 'MEETING_ADD_CITY' or dp == 'MEETING_ADD_STATE':
      #     write_file(filename, file_string, 'expected', reference_offset + int(expected[dp + "_S"]),  reference_offset + int(expected[dp + "_E"]))
      #     write_file(filename,  file_string, 'actual', start, end)
      return True
  return False


def compute_accuracy_using_offset(results, filename, localize_string):
  target_sheet = "T_PXYP_MEETING"
  if not os.path.basename(filename) in  filename_to_ref_no.keys():
    df.loc[get_index()] = [None, None, None, None, 'Not Present in Gold', None, filename]
    accuracy_dict["COUNT"] = accuracy_dict["COUNT"] - 1
    return
  rfid = filename_to_ref_no[os.path.basename(filename)]
  target_row = get_target_row(rfid, sheet_to_df_map, target_sheet)

  reference_offset = sheet_to_df_map["Document"].loc[rfid]["START_OFFSET"]
  file = open(filename, 'r')
  file_string = file.read().replace("\n", "\\n")
  file.close()


  for dp in dp_names:
    if check_value(target_row, results, dp, reference_offset, file_string, localize_string, filename):
      accuracy_dict[dp] = accuracy_dict[dp] + 1
    else:
      expected = target_row[dp]
      actual = results[dp] if dp in results else ""
      index = 0
      if df.empty == False:
        index = max(df.index) + 1
      tokenized_string, text = convert_htmlstring_to_tokens(localize_string)
      df.loc[index] = [dp, expected, actual, text, 'InCorrect Values', None, filename]


# input - directory name, file name
# returns localized_html_string or None if localization fails
def localize_file(file_location):

  p1 = re.compile(
    '(?i)(Notice)(?:(?!(?i)(Notice|Meeting|Stockholders|Shareholders)).)*?(?i)(Annual|Special)(?:(?!(?i)(Notice|Annual|Special|Stockholders|Shareholders)).)*?(?i)(Meeting)(?:(?!(?i)(Notice|Annual|Special|Meeting)).)*?(?i)(Stockholders|Shareholders|Members)')
  p2 = re.compile(
    '^(?=.*?(ANNUAL|Annual|annual|Special|special|SPECIAL))(?=.*?(Meeting|MEETING|meeting))(?=.*?(STOCKHOLDERS|SHAREHOLDERS|Shareholders|Stockholders|stockholders|shareholders)).+ ')
  p2_1 = re.compile('^(?=.*?(?i)(notice))(?=.*?(?i)(ANNUAL||SPECIAL))(?=.*?(?i)(meeting)).+ ')
  p2_2 = re.compile('^(?=.*?(?i)(notice))(?=.*?(?i)(meeting))(?=.*?(?i)(stockholders|shareholders|members)).+ ')

  with open(file_location, "r", encoding='utf-8-sig') as fp:

    file_string = fp.read().replace('\n', '\\n')
    matches = p1.finditer(file_string)
    match_success = False

    # iterate through the matches in the string
    for match in matches:
      result = (match.span()[0], match.span()[0] + 10000)
      if result:
        first_search = file_string[result[0]:result[1]]
        if end_condition(file_string[result[0]:result[1]]):
          match_success = True
          break
    if match_success:
      # return localized string
      return file_string[result[0]:result[1]]

    # fallback procedure with other regex
    else:
      m = p2.search(file_string)
      if not m:
        m = p2_1.search(file_string)
      if not m:
        m = p2_2.search(file_string)
      if m:
        match_string = file_string[m.span(1)[0]: m.span(1)[0] + 10000]
        m_am = end_condition(match_string)
        if m_am:
          # return localized string
          return match_string

    return None

def end_condition(text):
  p4_1 = re.compile(' ?([AaPp][.][Mm])')
  p4_2 = re.compile('([0-1]?[0-9]|2[0-3]):[0-5][0-9]')
  p4_3 = re.compile('20\d\d')
  #p4_4 = re.compile('\s+\d\s+')
  if p4_1.search(text) or p4_2.search(text) or p4_3.search(text):
    return True
  return False


def write_file(filename, filestr, obj, start_offset, end_offset):
  file = open("debug\\"+os.path.basename(filename)+"_"+obj+".html", "w")
  file.write("<html><body>")
  file.write(filestr[start_offset - 2000: start_offset - 20])
  file.write("<font color='red'>" + filestr[start_offset - 20: end_offset + 20] + "</font>")
  file.write(filestr[end_offset +20: end_offset + 1500])
  file.write("</body></html>")
  file.close()

#  save_logs(return_data, file_location, localized_string, filename_to_ref_no, sheet_to_df_map)

def compute_country(result):
  if 'MEETING_ADD_STATE' in result.keys():
    if result['MEETING_ADD_STATE'] in dict_country.keys():
      result['MEETING_ADD_COUNTRY'] = dict_country[result['MEETING_ADD_STATE']]
    else:
      print(result['MEETING_ADD_STATE'])
      if 'MEETING_ADD_CITY' in result.keys() and result['MEETING_ADD_CITY'] in dict_city.keys():
        result['MEETING_ADD_COUNTRY'] = dict_city[result['MEETING_ADD_CITY']]

def compute_state(return_data, keywords):
  for state in keywords:
    # if 'MEETING_ADD_CITY' in return_data.keys():
    #
    if state in dict_country.keys():
      return_data['MEETING_ADD_STATE'] = state


def compute_city(return_data, keywords):
  for city in keywords:
    if city in dict_city.keys():
      return_data['MEETING_ADD_CITY'] = city


def classify_using_spacy(localized_string, return_data):
  tokenized_string, normal_text = convert_htmlstring_to_tokens(localized_string)
  nlp = spacy.load('en_core_web_sm')
  doc = nlp(normal_text.strip())
  lt = []
  for ent in doc.ents:
    if ent.label_ == 'GPE':
      lt.append(ent.text)
  if not 'MEETING_ADD_STATE' in return_data.keys():
      compute_state(return_data, lt)
  if not 'MEETING_ADD_CITY' in return_data.keys():
      compute_city(return_data, lt)

def pxy_meeting_full_proc(file_location):
  localized_string = localize_file(file_location)

  if not localized_string:
    accuracy_dict["LOCALIZED_COUNT"] = accuracy_dict["LOCALIZED_COUNT"] + 1
    accuracy_dict["COUNT"] = accuracy_dict["COUNT"] - 1
    index = get_index()
    df.loc[index] = [None, None, None, None, 'LOCALIZATION', None, file_location]
    return
  try:
    return_data = classify_text_for_pxy_info_dp(localized_string)
    classify_using_pretrained_NER(localized_string, return_data, df, file_location)
#    classify_using_spacy(localized_string, return_data)
    compute_country(return_data)
    if 'MEETING_ADD_CITY' not in return_data.keys() and 'MEETING_ADD_STATE' not in return_data.keys():
      index = get_index()
      accuracy_dict["BOTH_ABSENT"] = accuracy_dict["BOTH_ABSENT"] + 1
      df.loc[index] = [None, None, None, None, 'Both City and State Absent', None, file_location]
    compute_accuracy_using_offset(return_data, file_location, localized_string)
  except Exception as e: # work on python 3.x
    accuracy_dict["EXCEPTION_COUNT"] = accuracy_dict["EXCEPTION_COUNT"] + 1
    index = get_index()
    accuracy_dict["COUNT"] = accuracy_dict["COUNT"] - 1
    df.loc[index] = [None, None, None, None, 'EXCEPTION', str(e), file_location]


def get_index():
  index = 0
  if df.empty == False:
    index = max(df.index) + 1
  return index


def get_gold_standard():
  global filename_to_ref_no, sheet_to_df_map
  if args.optimize == 'save':
    filename_to_ref_no, sheet_to_df_map = initialize_dataframe(gold_standard_location)
    with open('data/gold_standard_fileref.pickle', 'wb') as handle:
      pickle.dump(filename_to_ref_no, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open('data/gold_standard_filemap.pickle', 'wb') as handle:
      pickle.dump(sheet_to_df_map, handle, protocol=pickle.HIGHEST_PROTOCOL)
  elif args.optimize == 'load':
    with open('data/gold_standard_fileref.pickle', 'rb') as handle:
      filename_to_ref_no = pickle.load(handle)
    with open('data/gold_standard_filemap.pickle', 'rb') as handle:
      sheet_to_df_map = pickle.load(handle)
  else:
    filename_to_ref_no, sheet_to_df_map = initialize_dataframe(gold_standard_location)


def get_country_mapping():
  global dict_country
  df_ = pd.read_csv(country_file)
  df_ = df_.dropna()
  dict_country = {}
  for index, row in df_.iterrows():
    dict_country[row[0]] = row[1]

def get_city_mapping():
  global dict_city
  df_ = pd.read_csv(city)
  df_ = df_.dropna()
  dict_city = {}
  for index, row in df_.iterrows():
    dict_city[row[0]] = row[1]



def parse_documents():
  global args, file, dir, gold_standard_location, filename_to_ref_no, sheet_to_df_map, country_file, city
  parser = argparse.ArgumentParser(description='data-point-extraction')
  group = parser.add_mutually_exclusive_group(required=True)
  group.add_argument('-f', '--file', type=str, help='Path to Full address')
  group.add_argument('-d', '--directory', type=str, help='Path to directory')
  parser.add_argument('-g', '--gold', type=str, help='Path to gold standard file', required=True)
  parser.add_argument('--optimize', type=str, help='Save Excel')
  parser.add_argument('--country', type=str, help='Country')
  parser.add_argument('--city', type=str, help='City')
  args = parser.parse_args()
  file = args.file
  dir = args.directory
  gold_standard_location = args.gold
  country_file = args.country
  city = args.city


if __name__ == "__main__":
  parse_documents()
  get_gold_standard()
  get_country_mapping()
  get_city_mapping()

  if file is not None:
      pxy_meeting_full_proc(file)
      print(accuracy_dict)
  elif dir is not None:
      regex = dir+'/*.txt'
      list_files = glob.glob(regex)

      for files in list_files:
        print(files)
        pxy_meeting_full_proc(files)
        accuracy_dict["COUNT"] = accuracy_dict["COUNT"] + 1

      for x in accuracy_dict:
        if x not in ["COUNT", "LOCALIZED_COUNT", "EXCEPTION_COUNT", "MISSING_GOLD", "VALUE_OFFSET_MISMATCH", "BOTH_ABSENT", "VIRTUAL"]:
          accuracy_dict[x] = accuracy_dict[x]/accuracy_dict["COUNT"] * 100

      postfix = "_644_pretrained_def14"
      with open('accuracy2/overall_accuracy'+postfix+'.csv', 'w') as f:
        for key in accuracy_dict.keys():
          f.write("%s,%s\n" % (key, accuracy_dict[key]))

      df.to_csv('accuracy2/analysis'+postfix+'.csv')

